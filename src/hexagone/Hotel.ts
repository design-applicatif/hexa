import { ReceptionPort } from "./ReceptionPort";

export class Hotel {
  constructor(private readonly receptionPort: ReceptionPort) { }

  recupererChambres() {
    return this.receptionPort.recupererChambres();
  }

  fixerPrixChambreRDC(nouveauPrix: number) {
    var listeChambres = this.receptionPort.recupererChambres()
    for (const chambre of listeChambres) {
      chambre.modifierPrix(nouveauPrix);
    }
    return this.receptionPort.mettreAJourChambres(listeChambres);
  }
}
