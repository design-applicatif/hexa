import { Chambre } from "./Chambre";
import { ReceptionPort } from "./ReceptionPort";

export class ReceptionTestAdapter implements ReceptionPort {
  constructor(
    public listeChambres: Chambre[],
  ) { }

  recupererChambres(): Chambre[] {
    return this.listeChambres;
  }

  mettreAJourChambres(vouvelleListeChambres: Chambre[]): void {
    this.listeChambres
  }
}
