import { describe } from "node:test";
import { Chambre } from "./Chambre";
import { Hotel } from "./Hotel";
import { ReceptionTestAdapter } from "./ReceptionTestAdapter";


describe("Récupérer chambres", () => {
  describe(`Quand il n'y a pas de chambres dans l'hôtel`, () => {
    it("doit retourner une liste vide", () => {
      // given
      const hotel = new Hotel(new ReceptionTestAdapter([]));
      // when
      const listeDesChambres = hotel.recupererChambres();
      // then
      expect(listeDesChambres).toEqual([]);
    });
  });

  describe(`Quand il y a une chambre dans l'hôtel`, () => {
    it("doit retourner une liste avec une chambre", () => {
      // given
      const hotel = new Hotel(new ReceptionTestAdapter([new Chambre(0, 1, 50)]));
      const uneChambre = new Chambre(0, 1, 50);
      // when
      const listeDesChambres = hotel.recupererChambres();
      // then
      expect(listeDesChambres).toEqual([uneChambre]);
    });
  });
});
